#!/bin/bash

# Check if a percentage parameter is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <brightness_percentage>"
    exit 1
fi

# Get the max brightness value
max_brightness=$(cat /sys/class/backlight/rpi_backlight/max_brightness)

# Calculate the desired brightness based on the percentage
percentage="$1"
brightness_value=$((percentage * max_brightness / 100))

# Check if the calculated brightness is within the valid range
if [ "$brightness_value" -gt "$max_brightness" ]; then
    echo "Invalid brightness value. Use a percentage between 0 and 100."
    exit 1
fi

# Set the calculated brightness value
sudo bash -c "echo $brightness_value > /sys/class/backlight/rpi_backlight/brightness"

echo "Brightness set to $percentage%"

