#!/usr/bin/python
import os
import sys
import time
import pygame
import threading
import subprocess
from random import uniform
from random import randint
from datetime import datetime
from pygame._sdl2 import touch

screen_off_time = datetime.strptime("17:00:00", "%H:%M:%S").time()
screen_on_time = datetime.strptime("08:00:00", "%H:%M:%S").time()

print(os.getcwd())

pygame.init()

font_exit = pygame.font.SysFont('Comic Sans MS', 30)
text_exit = font_exit.render("X", False, (255, 0, 0))

font_shad = pygame.font.SysFont('quicksandmedium', 102)
text_shad = font_shad.render("Taylor Rhodes", False, (100, 100, 100))

font_name = pygame.font.SysFont('quicksandmedium', 100)
text_name = font_name.render("Taylor Rhodes", False, (0, 0, 0))

screen_width = 800
screen_height = 480
size = width, height = screen_width, screen_height
black = 0, 0, 0

screen = pygame.display.set_mode(size)
pygame.mouse.set_visible(False)

has_touchdevice = touch.get_num_devices() > 0

print("Has Touch Device: " + str(has_touchdevice))

image_folder = './images'
image_files = [f for f in os.listdir(image_folder) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
images = [pygame.image.load(os.path.join(image_folder, img)) for img in image_files]
scaled_images = [pygame.transform.scale(img, (screen_width, screen_height)) for img in images]

current_image = 0
next_image = (current_image + 1) % len(scaled_images)
start_time = pygame.time.get_ticks()
image_interval = 10000
fade_alpha = 0
fade_speed = 15  # Adjust this value to control the fading speed

clock = pygame.time.Clock()

def draw_images():
	screen.fill(black)

	# Draw current image
	screen.blit(scaled_images[current_image], (0, 0))

	# Draw next image with fading effect
	next_image_surface = pygame.Surface((screen_width, screen_height))
	next_image_surface.fill((0, 0, 0))
	next_image_surface.blit(scaled_images[next_image], (0, 0))
	next_image_surface.set_alpha(fade_alpha)
	screen.blit(next_image_surface, (0, 0))
	screen.blit(text_shad, (53,18))
	screen.blit(text_name, (60,20))

	pygame.display.flip()

def turn_off_screen():
    subprocess.run(["vcgencmd", "display_power", "0"])  # Turn off the screen

def turn_on_screen():
    subprocess.run(["vcgencmd", "display_power", "1"])  # Turn on the screen

def screen_control_thread():
    while True:
        current_time = datetime.now().time()

        if current_time >= screen_off_time and current_time < screen_on_time:
            turn_off_screen()

        elif current_time >= screen_on_time or current_time < screen_off_time:
            turn_on_screen()

        time.sleep(60)  # Wait for 1 minute before checking again

screen_thread = threading.Thread(target=screen_control_thread)
screen_thread.daemon = True
screen_thread.start()

running = True
while running:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
		elif event.type == pygame.FINGERDOWN:
			print("Touch detected")
			x = round(event.x * width)
			y = round(event.y * height)
			if x < 75 and y < 75:
				running = False
		elif event.type == pygame.MOUSEBUTTONDOWN:
			print("Mouse Click Detected")
			print("Position: {}, {}".format(event.pos[0], event.pos[1]))
			print(event)
			running = False

	elapsed_time = pygame.time.get_ticks() - start_time
	if elapsed_time >= image_interval:
		start_time = pygame.time.get_ticks()
		current_image = next_image
		next_image = (current_image + 1) % len(scaled_images)
		fade_alpha = 0  # Reset the fade alpha for the new image

	# Update fading effect
	fade_alpha += fade_speed
	if fade_alpha > 255:
		fade_alpha = 255

	draw_images()

	clock.tick(60)

pygame.quit()
