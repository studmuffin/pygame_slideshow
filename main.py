#!/usr/bin/python
import os
import sys
import time
import pygame
from random import uniform
from random import randint
from pygame._sdl2 import touch

print(os.getcwd())

pygame.init()

font_exit = pygame.font.SysFont('Comic Sans MS', 30)
text_exit = font_exit.render("X", False, (255, 0, 0))

font_shad = pygame.font.SysFont('quicksandmedium', 102)
text_shad = font_shad.render("Taylor Rhodes", False, (100, 100, 100))

font_name = pygame.font.SysFont('quicksandmedium', 100)
text_name = font_name.render("Taylor Rhodes", False, (0, 0, 0))

screen_width = 800
screen_height = 480
size = width, height = screen_width, screen_height
black = 0, 0, 0

screen = pygame.display.set_mode(size)
pygame.mouse.set_visible(False)

has_touchdevice = touch.get_num_devices() > 0

print("Has Touch Device: " + str(has_touchdevice))

image_folder = './images'
image_files = [f for f in os.listdir(image_folder) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
images = [pygame.image.load(os.path.join(image_folder, img)) for img in image_files]
scaled_images = [pygame.transform.scale(img, (screen_width, screen_height)) for img in images]

current_image = 0
image_interval = 5000
start_time = pygame.time.get_ticks()

while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT: sys.exit()
		elif event.type == pygame.FINGERDOWN:
			print("Touch detected")
			x = round(event.x * width)
			y = round(event.y * height)
			if x < 75 and y < 75:
				sys.exit()
		elif event.type == pygame.MOUSEBUTTONDOWN:
			print("Mouse Click Detected")
			print("Position: {}, {}".format(event.pos[0], event.pos[1]) )
			print(event)
			sys.exit()

	elapsed_time = pygame.time.get_ticks() - start_time
	if elapsed_time >= image_interval:
		start_time = pygame.time.get_ticks()
		current_image = (current_image + 1) % len(scaled_images)

	screen.fill(black)
	screen.blit(scaled_images[current_image], (0,0))
#	screen.blit(text_exit, (10,10))
	screen.blit(text_shad, (54,18))
	screen.blit(text_name, (60,20))
	pygame.display.flip()


pygame.quit()
